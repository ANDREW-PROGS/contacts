/*****************************************************************************/
//5_1_21 cub173.c--> cub200.c functional
// amended multmatrix function
//declare SPRSYS as char
//revised bodyface table
//uses Draw triangle and sintab81 code from bll
//courtesy of B.Schick @BS42
//declares MATHX registers as extern int MATHX;
//and .global in
//sintab_8.asm sine table 256 16-bit signed entries (cosine as well)
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <string.h>
#include <stdlib.h>

extern char lynxtgi[];
extern char lynxjoy[];
extern char SinTablo[];
extern char SinTabhi[];
extern int MATHD; 
extern int MATHH; 
extern int MATHB;
extern int MATHL;
extern int MATHK;
extern int MATHM;
extern char SPRSYS; 
extern void Dtangle(void);
extern char cubecontact[]; 
extern int x1;
extern int y1;
extern int x2;
extern int y2;
extern int x3;
extern int y3;
extern char tri_scb2_color;
extern char tri_scb_color;

typedef struct {
    unsigned char b0;
    unsigned char b1;
    unsigned char b2;
    void *next;
    void *bitmap;
    int posx, posy, sizex, sizey;
    int stretch, tilt;
    char palette[8];
} sprite_t;

	sprite_t contact = {
	0xc5, 0x30, 0x01,
	0,
	&cubecontact,
	0, 0,
	0x100, 0x100,
	0x0000, 0x0000,
	{0x01, 0x33, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

char palette1[] = {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x03,0x02,0x02,0x06,0x05,
	0x60,0x80,0x72,0x62,0x93,0x96,0xF5,0xC5,0xB9,0xF9,0x83,0xB3,0xC6,0xF8,0xC3,0xD4};

#define VERTICES 8 //for testing 
// Moving Polygons 
// Proposed 3d environment for Lynx and CC65


typedef struct {
	signed int elemnt1;
	signed int elemnt2;
	signed int elemnt3;
} execrot;

	execrot execrotx;
	execrot execroty;
	execrot execrotz;
	int actual_angle[3];
	char angular[3];
	char vertices = 0x08;
	signed int tripoint[VERTICES*3];
	int xorigin[3];
	int theto[3]; // representation of -pi/2>theta>pi/2		
	unsigned char axis = 3; 
	unsigned char vertex = 1;

	char l; //temporary addition ,duplicated below db 17_5_20
	char j;
	signed int accum;
	signed int zstore[]= {0x00FF,0x00FF,0,0,0,0};
	signed int *(*temp);//dbug 17_5_20
	signed int *(*temp1);
	signed int temp3;
	int *(*point1);	
 	int *(*sidevector0);// db was signed int 24_9_20
	int *(*sidevector1);// db 3_8_20
	int *(*sidevector2); 
	int *(*sidevector3); 
	int *(*sidevector4); 
	int *(*sidevector5);
//cube database here
typedef struct {
	signed int xcoord;
	signed int ycoord;
	signed int zcoord;	
} cubecorner;
cubecorner v0 = { 0x0010, 0x0010, 0xFFF0};
cubecorner v1 = { 0x0010, 0xFFF0, 0xFFF0};
cubecorner v2 = { 0xFFF0, 0x0010, 0xFFF0}; 
cubecorner v3 = { 0xFFF0, 0xFFF0, 0xFFF0};
cubecorner v4 = { 0x0010, 0xFFF0, 0x0010};
cubecorner v5 = { 0xFFF0, 0xFFF0, 0x0010};
cubecorner v6 = { 0xFFF0, 0x0010, 0x0010};
cubecorner v7 = { 0x0010, 0x0010, 0x0010};

typedef struct {
	signed int *param1;
	signed int *param2;
	signed int *param3;
	signed int *param4;
	int colparam; // colour byte 
} bodyface;

	bodyface face1 = { &v0.xcoord, &v1.xcoord, &v2.xcoord, &v3.xcoord, 0x0001};
	bodyface face2 = { &v1.xcoord, &v0.xcoord, &v4.xcoord, &v7.xcoord, 0x0002};
	bodyface face3 = { &v0.xcoord, &v2.xcoord, &v7.xcoord, &v6.xcoord, 0x0003};
	bodyface face4 = { &v2.xcoord, &v3.xcoord, &v6.xcoord, &v5.xcoord, 0x0004};
	bodyface face5 = { &v3.xcoord, &v1.xcoord, &v5.xcoord, &v4.xcoord, 0x0005};
	bodyface face6 = { &v5.xcoord, &v6.xcoord, &v4.xcoord, &v7.xcoord, 0x0006};	

signed int Sin(char ang,char sgn)
{ 
	int tval;
	tval = ((SinTabhi[ang] << 8 ) | SinTablo[ang]);
	if (!sgn) { 
	tval = -tval;	
}
	return tval;
} 
//similar for cos (different offset!)
signed int cos(char ang,char sgn)
{ 
	int tval;
	ang = (ang + 0x40); //ang is char >255 code 10_11_19
	tval = ((SinTabhi[ang] << 8 ) | SinTablo[ang]);
	if (!(sgn)) {
	tval = -tval;
}
	return tval;
} 
///////////////////////////////////////////////////////
void execmove(signed int *value, signed int distance)
{
	*value += distance;
}
//////////////////////////////////////////////////////
signed char projectedx (signed int view1, signed int view2)

{	
	signed char xview,zview;
	if (view2 & 0x8000) {
	view2 = view2 ^ 0xffff;
	view2 ++;
}	
	zview = 24; //value was24 db16_10_20
	xview = (view1 * zview) / (zview + view2);
	//centre origin at middle of screen
	return xview; 
}
signed char projectedy(signed int view1, signed int view2) //dbug 4_8_19
{
	signed char yview,zview; 
	if (view2 & 0x8000) {
	view2 = view2 ^ 0xffff;
	view2 ++;
}	
	zview = 24;
	yview = (view1 * zview) / (zview + view2);
	return yview;
}

void facerender(int *(*cpointer)) 
{
	signed int par_x;
	signed int par_y;
	signed int par_z;
	signed int *t2pointer;
	t2pointer = *cpointer;
	par_x = *t2pointer;
	par_y = *(t2pointer +1);
	par_z = *(t2pointer + 2);
	x1 = 80 + (projectedx(par_x,par_z));
	y1 = 51 + (projectedy(par_y,par_z));
	cpointer ++;
	t2pointer = *cpointer;
	par_x = *t2pointer;
	par_y = *(t2pointer +1);
	par_z = *(t2pointer + 2);
	x2 = 80 + (projectedx(par_x,par_z));
	y2 = 51 + (projectedy(par_y,par_z));	
	cpointer ++;
	t2pointer = *cpointer;
	par_x = *t2pointer;
	par_y = *(t2pointer +1);
	par_z = *(t2pointer +2);
	x3 = 80 + (projectedx(par_x,par_z));
	y3 = 51 + (projectedy(par_y,par_z));
	cpointer +=2;
	tri_scb_color = (char) *cpointer;
	tri_scb2_color = (char) *cpointer;
	Dtangle();
	while (tgi_busy()){ 
}	
	//shortcut technique follows
	cpointer -=3;
	t2pointer = *cpointer;
	par_x = *t2pointer;
	par_y = *(t2pointer +1);
	par_z = *(t2pointer +2);
	x1 = 80 + (projectedx(par_x,par_z));
	y1 = 51 + (projectedy(par_y,par_z));
	cpointer ++;
	t2pointer = *cpointer;
	par_x = *t2pointer;
	par_y = *(t2pointer +1);
	par_z = *(t2pointer +2);
	x2 = 80 + (projectedx(par_x,par_z));
	y2 = 51 + (projectedy(par_y,par_z));
	cpointer ++;
	t2pointer = *cpointer;
	par_x = *t2pointer;
	par_y = *(t2pointer +1);
	par_z = *(t2pointer +2);
	x3 = 80 + (projectedx(par_x,par_z));
	y3 = 51 + (projectedy(par_y,par_z));
	cpointer ++;
	tri_scb_color = (char) *cpointer;
	tri_scb2_color = (char) *cpointer;
	Dtangle();
	while (tgi_busy()){ 
}		
}	
	
void setexecrot(char theta) 
//axis should be in scope.
{
	switch(axis)
{
	case (2) : { // data is for rotate about y axis.
	execrotx.elemnt1 = cos(theta,1);
	execrotx.elemnt2 = 0;
	execrotx.elemnt3 = Sin(theta,1);  
	execroty.elemnt1 = 0;
	execroty.elemnt2 = 0x100;
	execroty.elemnt3 = 0;  
	execrotz.elemnt1 = Sin(theta,0);
	execrotz.elemnt2 = 0;
	execrotz.elemnt3 = cos(theta,1);  
} 
	break ; //or return 
	case (1) : { // about the x axis.
	execrotx.elemnt1 = 0x100;
	execrotx.elemnt2 = 0;
	execrotx.elemnt3 = 0; 
	execroty.elemnt1 = 0;
	execroty.elemnt2 = cos(theta,1);
	execroty.elemnt3 = Sin(theta,0);
	execrotz.elemnt1 = 0;
	execrotz.elemnt2 = Sin(theta,1);
	execrotz.elemnt3 = cos(theta,1);  
}
	break ; //or return 
	case (3) : { //about the z axis.
	execrotx.elemnt1 = cos(theta,1);
	execrotx.elemnt2 = Sin(theta,0);
	execrotx.elemnt3 = 0; 
	execroty.elemnt1 = Sin(theta,1);
	execroty.elemnt2 = cos(theta,1);
	execroty.elemnt3 = 0;  
	execrotz.elemnt1 = 0;
	execrotz.elemnt2 = 0;
	execrotz.elemnt3 = 0x100; 
}
	break ; //or return 
}
	return ;
}

//I include checks for multiply by zero.
void multmatrix(signed int *vpointer) 
{
signed int tempx, tempy, tempz;
	tempx = *vpointer; 
	tempy = *(vpointer+1);
	tempz = *(vpointer+2);
// Initialise the accumulator 0xJKLM
//by writing a 0 to K and M.
// 16bit answer given by contents of 0xKL 
//the multiply and accumulate -maths

	SPRSYS = SPRSYS | 0xc0; //turn acc. and signed math on
	MATHM = 0x0; 
	MATHK = 0x0;
	if (execrotx.elemnt1) {
	MATHD = execrotx.elemnt1;
	MATHB = tempx; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotx.elemnt2) {
	MATHD = execrotx.elemnt2;
	MATHB = tempy; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotx.elemnt3) {
	MATHD = execrotx.elemnt3;
	MATHB = tempz; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
} 
//	tripoint[vertex] = MATHL; // care needed y and z rotation use cubecorner.xcoord initial values 
//placing revised values must be done after all 3 subtotals have been found.
//  0xJKLM is contiguous and in proper longword order in hardware regs.
	*vpointer = MATHL;	
	MATHM = 0x0000;
	MATHK = 0x0000;
	// initialise accumulator (JKLM)
	
	if (execroty.elemnt1) {
	MATHD = execroty.elemnt1;
	MATHB = tempx;
}
	while (tgi_busy()) { 
}
	//MATHB = tempx; //calculation starts with write to _MATHA
	if (execroty.elemnt2) {
	MATHD = execroty.elemnt2;
	MATHB = tempy; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execroty.elemnt3) {
	MATHD = execroty.elemnt3;
	MATHB = tempz; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
//	tripoint[vertex+1] = MATHL; //new y calc'd
	*(vpointer+1) = MATHL;
// initialise accumulator (JKLM)
	MATHM = 0x0000;
	MATHK = 0x0000;
	if (execrotz.elemnt1) {
	MATHD = execrotz.elemnt1;
	MATHB = tempx; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotz.elemnt2) {
	MATHD = execrotz.elemnt2;
	MATHB = tempy; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotz.elemnt3) {
	MATHD = execrotz.elemnt3;
	MATHB = tempz; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
//	tripoint[vertex+2] = MATHL; //new z calc'd
	*(vpointer+2) = MATHL; //check 24_7_2020
	SPRSYS = SPRSYS & 0x3f; // turn accumulator and signed math off
}

////////////////painters method scratch 2_11_2020
void execsort( void )
{
// put mean values of z for each face in zstore[]
	signed int *temp3; 
	signed int temp4; 
	temp = &face1.param1; 
	temp1 = &face1.param4; 
	for (l = 0;l < 6;l++)
{ 	
	temp3 = *temp + 2; //code to point to z amount 2_1_20
	accum = *temp3;

	temp3 = *temp1 + 2; //code to point to z amount 2_1_20
	accum = accum + *temp3;
	if (accum & 0x8000){
		accum = accum ^ 0xFFFF;
		accum++;
	}
	accum = (accum/2);
	accum = accum ^ 0xFFFF;//*********accum values arranged to be negative
	accum++;
	zstore[l] = accum;
	temp = temp + 5;// 5 as in 5 times ints-check src 
	temp1 = temp1 + 5;
}
/////////////////////////
	sidevector0 = &face1.param1;
	sidevector1 = &face2.param1;
	sidevector2 = &face3.param1;
	sidevector3 = &face4.param1;
	sidevector4 = &face5.param1;
	sidevector5 = &face6.param1;

//sort is here.improvement will be to use inner loop and outer loop.
	for (l = 0;l < 6;l++) 
{	
	if (zstore[0] < zstore[1])
{
	temp4 = zstore[0];
	zstore[0] = zstore[1];
	zstore[1] = temp4;
	point1 = sidevector0; 
	sidevector0 = sidevector1;
	sidevector1 = point1;
}
 
	if (zstore[1] < zstore[2])
{
	temp4 = zstore[1];
	zstore[1] = zstore[2];
	zstore[2] = temp4;
	point1 = sidevector1; 
	sidevector1 = sidevector2;
	sidevector2 = point1;
}
	if (zstore[2] < zstore[3])
{
	temp4 = zstore[2];
	zstore[2] = zstore[3];
	zstore[3] = temp4;
	point1 = sidevector2; 
	sidevector2 = sidevector3;
	sidevector3 = point1;
}
	if (zstore[3] < zstore[4])
{
	temp4 = zstore[3];
	zstore[3] = zstore[4];
	zstore[4] = temp4;
	point1 = sidevector3; 
	sidevector3 = sidevector4;
	sidevector4 = point1;
}
	if (zstore[4] < zstore[5])
{
	temp4 = zstore[4];
	zstore[4] = zstore[5];
	zstore[5] = temp4;
	point1 = sidevector4; 
	sidevector4 = sidevector5;
	sidevector5 = point1;
	
}
}

}
////
void main()
{
	char m;
	tgi_install(&lynx_160_102_16);
	tgi_install(&lynxtgi);
    joy_install(&lynx_stdjoy);
     tgi_init();
     CLI();

	tgi_setpalette(palette1); 
/***************************************/

	axis = 3; //axis =3,z axis 
	setexecrot(32); 
	multmatrix(&v0.xcoord);
	multmatrix(&v1.xcoord);
	multmatrix(&v2.xcoord);
	multmatrix(&v3.xcoord);
	multmatrix(&v4.xcoord);
	multmatrix(&v5.xcoord);
	multmatrix(&v6.xcoord);
	multmatrix(&v7.xcoord);


	axis = 2; //2<y-axis>.
	setexecrot(16); // 32 is 90degrees

	multmatrix(&v0.xcoord);
	multmatrix(&v1.xcoord);
	multmatrix(&v2.xcoord);
	multmatrix(&v3.xcoord);
	multmatrix(&v4.xcoord);
	multmatrix(&v5.xcoord);
	multmatrix(&v6.xcoord);
	multmatrix(&v7.xcoord);
	contact.posx = 0x0;
	contact.posy = 0x0;
	contact.sizex = 0x100;
	contact.sizey = 0x100;
	for (m = 0; m < 64; m++) {		
	execmove(&v0.zcoord,0xFFEC); 
	execmove(&v1.zcoord,0xFFEC);
	execmove(&v2.zcoord,0xFFEC); 
	execmove(&v3.zcoord,0xFFEC);
	execmove(&v4.zcoord,0xFFEC);
	execmove(&v5.zcoord,0xFFEC);
	execmove(&v6.zcoord,0xFFEC);
	execmove(&v7.zcoord,0xFFEC);
	execsort();
	tgi_setcolor(0); 
	tgi_bar(0,0,159,101); // clear screen 
	while (tgi_busy()){
}	
	tgi_sprite(&contact);
//	facerender(sidevector5);
//	facerender(sidevector4); 
//	facerender(sidevector3); 
	facerender(sidevector2); 	
	facerender(sidevector1);
	facerender(sidevector0);
	for (temp3=0; temp3<0x7fff; temp3++){
}
	tgi_updatedisplay(); // swapbuffers	
	execmove(&v0.zcoord,0x0014); 
	execmove(&v1.zcoord,0x0014);
	execmove(&v2.zcoord,0x0014); 
	execmove(&v3.zcoord,0x0014);
	execmove(&v4.zcoord,0x0014);
	execmove(&v5.zcoord,0x0014);
	execmove(&v6.zcoord,0x0014);
	execmove(&v7.zcoord,0x0014);
	multmatrix(&v0.xcoord);
	multmatrix(&v1.xcoord);
	multmatrix(&v2.xcoord);
	multmatrix(&v3.xcoord);
	multmatrix(&v4.xcoord);
	multmatrix(&v5.xcoord);
	multmatrix(&v6.xcoord);
	multmatrix(&v7.xcoord);
}

	for(;;){
}
}



