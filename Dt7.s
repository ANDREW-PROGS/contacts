;*******************
;Dt6rel-->Dt7 additional equates incl for cuboid code 18_8_2019
;transposed Matn regs from bll, and correct literal sprite data
; y3>y2>y1
; x1,y1 integer first coord stored in ZP
; x2,y2 integer second coord stored in ZP
; x3,y3 ditto
	.global _Dtangle
	.global _x1,_y1,_x2,_y2,_x3,_y3
	.global _MATHD,_MATHE,_MATHF,_MATHC,_MATHB
	.global _MATHA,_MATHH,_MATHJ,_MATHP,_MATHN  
 	.global _MATHK,_MATHL,_MATHM,_SPRSYS
	.global _tri_scb2_color
	.global _tri_scb_color
           
_MATHD	   =$FC52
_MATHE	  = $FC63
_MATHF	  = $FC62
_MATHC       = $FC53
_MATHB       = $FC54
_MATHA       = $FC55
_MATHH 	  = $FC60
_MATHJ	  = $FC6F
_MATHP	  = $FC56
_MATHN	  = $FC57
_MATHK	  = $FC6E
_MATHL	  = $FC6D
_MATHM	  = $FC6C
_SPRSYS	  = $FC92
_MATHG	  = $FC61
.segment "DATA"
_tiltAB: .word 0 
_tiltAC: .word 0 
_tiltBC: .word 0 
_x1: .word 0
_y1: .word 0            
_x2: .word 0           
_y2: .word 0 
_x3: .word 0
_y3: .word 0
_temp: .word 0 
 

.segment "RODATA"  
_Dtangle:
 ; sort points
_cmpws1:		lda _y1+1
			cmp _y2+1
			bmi _yes1
			bne _no1
			lda _y1
			cmp _y2
			bcc _yes1
			beq _yes1
_no1:			clc
			jmp _cmpdone1
_yes1:		sec
_cmpdone1:		bcs _doupp1 
			ldx _x1; else swap _x1 and _x2
                lda _x2
                stx _x2
                sta _x1
                ldx 1+_x1
                lda 1+_x2
                stx 1+_x2
                sta 1+_x1
			ldx _y1
                lda _y2
                stx _y2
                sta _y1
                ldx 1+_y1
                lda 1+_y2
                stx 1+_y2
                sta 1+_y1
_doupp1:		lda _y1+1
			cmp _y3+1
			bmi _yes2
			bne _no2
			lda _y1
			cmp _y3
			bcc _yes2
			beq _yes2
_no2:			clc
			jmp _cmpdone2
_yes2:		sec
_cmpdone2:		bcs _doupp2
			ldx _x1;
                lda _x3
                stx _x3
                sta _x1
                ldx 1+_x1
                lda 1+_x3
                stx 1+_x3
                sta 1+_x1
			ldx _y1
                lda _y3
                stx _y3
                sta _y1
                ldx 1+_y1
                lda 1+_y3
                stx 1+_y3
                sta 1+_y1
_doupp2:        lda _y2+1
			cmp _y3+1
			bmi _yes3
			bne _no3
			lda _y2
			cmp _y3
			bcc _yes3
			beq _yes3
_no3:			clc
			jmp _cmpdone3
_yes3:		sec
_cmpdone3:		bcs _doupp3
			ldx _x3
                lda _x2
                stx _x2
                sta _x3
                ldx 1+_x3
                lda 1+_x2
                stx 1+_x2
                sta 1+_x3
			ldx _y3
                lda _y2
                stx _y2
                sta _y3
                ldx 1+_y3
                lda 1+_y2
                stx 1+_y2
                sta 1+_y3
;
;
_doupp3:		ldy #$ff
                sec
                lda _y2
                sbc _y1

                sta _MATHP
                stz _MATHN
			beq _path1
			inc
_path1:		sta _tri_scb_ysize+1
                stz _MATHH
                stz _MATHG
                sec
                lda _x2
                sbc _x1
                tax
                lda _x2+1
                sbc _x1+1
                bpl _path1a
                  iny
                  pha
                  txa
                  eor #$ff
                  tax
                  pla
                  eor #$ff
                  inx
                  bne _path1a
                  inc
_path1a:
                stx _MATHF ;(mathn) looks like a divide
                sta _MATHE
                ;WAITSUZY 
_waitsuzi1:	bit $FC92
			bmi _waitsuzi1
                tya
			bne _path2
                  sec
                  sbc _MATHC ; cb pair
                  tax
                  tya
                  sbc _MATHB
                  dey
			jmp _path2e
_path2:		  ldx _MATHC
                  lda _MATHB
_path2e:        stx _tiltAB
                sta _tiltAB+1
;;;;;;;;;;;;;;;;;;
                sec
                lda _y3
                sbc _y1
                sta _MATHP
                stz _MATHN
                stz _MATHH
                stz _MATHG
                sec
                lda _x3
                sbc _x1
                tax
                lda _x3+1
                sbc _x1+1
                bpl _path2f
                  iny
                  pha
                  txa
                  eor #$ff
                  tax
                  pla
                  eor #$ff
                  inx
                  bne _path2f
                  inc
_path2f:
                stx _MATHF
                sta _MATHE
                ;WAITSUZY
_waitsuzi2:	bit $FC92
			bmi _waitsuzi2
                tya
                bne _path3
                  sec
                  sbc _MATHC
                  tax
                  tya
                  sbc _MATHB
                  dey
			jmp _path4
_path3:         ldx _MATHC
                  lda _MATHB
_path4:		stx _tiltAC
                sta _tiltAC+1
                sec
                lda _y3
                sbc _y2
                sta _MATHP
                stz _MATHN
			sta _tri_scb2_ysize+1
                stz _MATHH
                stz _MATHG
                sec
                lda _x3
                sbc _x2
                tax
                lda _x3+1
                sbc _x2+1
                bpl _path4a
                  iny
                  pha
                  txa
                  eor #$ff
                  tax
                  pla
                  eor #$ff
                  inx
                  bne _path4a
                  inc
_path4a:
                stx _MATHF
                sta _MATHE
                ;WAITSUZY
_waitsuzi3:	bit $FC92
			bmi _waitsuzi3
                tya
                bne _path5b
                  sec
                  sbc _MATHC
                  tax
                  tya
                  sbc _MATHB
                  ;dey 
			jmp _path6
_path5b:         ldx _MATHC
                  lda _MATHB
_path6:		 stx _tiltBC
                sta _tiltBC+1
;;;;;;;;;;;
			lda _x1
			sta _tri_scb_x
			lda _x1+1
			sta _tri_scb_x+1
			lda _y1
			sta _tri_scb_y
			lda _y1+1
			sta _tri_scb_y+1
			lda _x3
			sta _tri_scb2_x
			lda _x3+1
			sta _tri_scb2_x+1
			lda _y3
			sta _tri_scb2_y
			lda _y3+1
			sta _tri_scb2_y+1
			lda #$0
			sta _tri_scb_xsize
			sta _tri_scb2_xsize
			lda #1
			sta _tri_scb_xsize+1
			sta _tri_scb2_xsize+1
;;;;;;          
			lda _tiltAC
			sta _temp
			lda _tiltAC+1
			sta _temp+1
_cmpws4:		lda _tiltAC+1
			cmp _tiltAB+1
			bmi _yes4
			bne _no4
			lda _tiltAC
			cmp _tiltAB
			bcc _yes4
			beq _yes4
_no4:			clc
			jmp _cmpdone4
_yes4:			sec
_cmpdone4:		bcs _doupp4
			;SWAP _tiltAB,_tiltAC 
			lda _tiltAB
			ldy _tiltAC
			sta _tiltAC
			sty _tiltAB
			lda _tiltAB+1
			ldy _tiltAC+1
			sta _tiltAC+1
			sty _tiltAB+1
_doupp4:		;SUBW _tiltAC,_tiltAB,.tri_scb_stretch
			;subtract 1# from 2nd# andstore in 3rd#
			sec
			lda _tiltAB 
			sbc _tiltAC
			sta _tri_scb_stretch
			lda _tiltAB+1
			sbc _tiltAC+1
			sta _tri_scb_stretch+1			
			lda _tiltAC
			sta _tri_scb_tilt
			lda _tiltAC+1
			sta _tri_scb_tilt+1
;;;;;;;;;;;;
_cmpws5:		lda _temp+1
			cmp _tiltBC+1
			bmi _yes5
			bne _no5
			lda _temp
			cmp _tiltBC
			bcc _yes5
			beq _yes5
_no5:			clc
			jmp _cmpdone5
_yes5:		sec
_cmpdone5:		bcs _doupp5 
			;SWAP _temp,_tiltBC 
			lda _temp
			ldy _tiltBC
			sta _tiltBC
			sty _temp
			lda _temp+1
			ldy _tiltBC+1
			sta _tiltBC+1
			sty _temp+1
_doupp5:		;SUBW _temp,_tiltBC,.tri_scb2_stretch
			sec						
			lda _tiltBC					
			sbc _temp					
			sta _tri_scb2_stretch
			lda _tiltBC+1
			sbc _temp+1
			sta _tri_scb2_stretch+1
			sec
			lda #0 
                sbc _tiltBC
                sta _tri_scb2_tilt
                lda #0
                sbc _tiltBC+1
			sta _tri_scb2_tilt+1
			lda _tri_scb_ysize+1
                ;_IFNE
			beq _path8
			lda #<_tri_scb
                  sta $fc10
                  lda #>_tri_scb
                  sta $fc11

                  lda #1
                  STA $FC91
                  lda #1
                  stz $fd90
_wait9:           STZ $FD91
                  bit $fc92
                  bne _wait9
                  STZ $FD90
			  jmp _path9
_path8:		lda _tri_scb2_ysize+1
                 ; _IFNE
			beq _path9
                inc _tri_scb2_ysize+1
                 ; _ENDIF
                ;_ENDIF
_path9:		 lda _tri_scb2_ysize+1 
                ;_IFNE
			beq _path11
			  lda #<_tri_scb2
                  sta $fc10
                  lda #>_tri_scb2
                  sta $fc11
                  lda #1
                  STA $FC91
                  lda #1
                  stz $fd90
_wait10:          STZ $FD91
                  bit $fc92
                  bne _wait10
                  STZ $FD90
                ;_ENDIF
_path11:         rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_trieveron1:     .byte 0
_tri_scb:        .byte $c0,$80|$30,0
                .word 0,_triimage
_tri_scb_x:      .word 0
_tri_scb_y:      .word 0
_tri_scb_xsize:  .word $100
_tri_scb_ysize:  .word $100
_tri_scb_stretch: .word 0
_tri_scb_tilt:   .word 0
_tri_scb_color:  .byte $a
;orig $3 2_2
_trieveron2:         .byte 0
_tri_scb2:       .byte $d0,$80|$30,0
                .word 0,_triimage
_tri_scb2_x:     .word 0
_tri_scb2_y:     .word 0
_tri_scb2_xsize: .word $100
_tri_scb2_ysize: .word $100
_tri_scb2_stretch: .word 0
_tri_scb2_tilt:  .word 0
_tri_scb2_color: .byte $a
;orig $e,3,6,4,9,a 2_2
_triimage:       .byte 2,$10,0
			 
			


                        

